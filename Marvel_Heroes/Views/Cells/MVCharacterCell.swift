//
//  MVCharacterCell.swift
//  Marvel_Heroes
//
//  Created by Antonio Muñoz Nieto on 11/05/2020.
//  Copyright © 2020 Antonio Muñoz Nieto. All rights reserved.
//

import UIKit

class MVCharacterCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView?
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
