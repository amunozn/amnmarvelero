//
//  MVAPIManager.swift
//  Marvel_Heroes
//
//  Created by Antonio Muñoz Nieto on 11/05/2020.
//  Copyright © 2020 Antonio Muñoz Nieto. All rights reserved.
//

import Foundation

struct MVApiManager{
    static let request = MVRequest(baseUrl: Config.baseUrl, privateKey: Config.Keys.marvelPrivate, publicKey: Config.Keys.marvelPublic)
}
