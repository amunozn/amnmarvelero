//
//  DataExtensions.swift
//  Marvel_Heroes
//
//  Created by Antonio Muñoz Nieto on 11/05/2020.
//  Copyright © 2020 Antonio Muñoz Nieto. All rights reserved.
//

import Foundation

extension Data {
    func toDictionary() -> [String: Any]? {
        let jsonError: NSError?
        do{
            if let dict = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any> {
                return dict
            }
        }catch let error as NSError {
            jsonError = error
            print(jsonError as Any)
        }
        return nil
    }
}
