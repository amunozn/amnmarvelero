//
//  MVCollectionContainerViewController.swift
//  Marvel_Heroes
//
//  Created by Antonio Muñoz Nieto on 11/05/2020.
//  Copyright © 2020 Antonio Muñoz Nieto. All rights reserved.
//

import UIKit

class MVCollectionContainer: MVBaseContainerViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pagerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let pictureCollectionViewController = self.contentViewController as? MVPictureCollectionViewController{
            pictureCollectionViewController.delegate = self
        }
    }
}

extension MVCollectionContainer: PictureCollectionViewControllerDelegate{
    func currentPageIndex(page: Int, pagesCount: Int){
        self.pagerLabel.text = "\(page)/\(pagesCount)"
    }
    func currentPageTitle(title: String){
        self.nameLabel.text = title
    }
}
